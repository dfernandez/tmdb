package com.example.tmdb.network.responses

import com.squareup.moshi.Json
import java.util.*

data class Token(
    val success: Boolean,
    @field:Json(name = "expires_at") val expiresAt: String,
    @field:Json(name = "request_token") val requestToken: String)