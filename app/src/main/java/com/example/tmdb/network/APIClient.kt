package com.example.tmdb.network

import com.example.tmdb.network.responses.Token


interface APIClient {
    suspend fun getNewToken(): Token
}