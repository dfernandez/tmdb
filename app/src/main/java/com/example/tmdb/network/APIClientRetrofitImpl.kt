package com.example.tmdb.network

import com.example.tmdb.BuildConfig
import com.example.tmdb.network.responses.Token
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import retrofit2.Response
import javax.inject.Inject
import kotlin.coroutines.suspendCoroutine

//class APIClientRetrofitImpl@Inject constructor(private val retrofitService: RetrofitService): APIClient {
//    override suspend fun getNewToken(): Token {
//        CoroutineScope(Dispatchers.IO).launch {
//            retrofitService.getNewToken(BuildConfig.TMDB_API_KEY)
//
//            client.newCall(request).enqueue(object : Callback {
//                override fun onFailure(call: Call, e: IOException) {
//                    cont.resumeWithException(e)
//                }
//                override fun onResponse(call: Call, response: Response) {
//                    cont.resume(response.body()?.string() ?: "")
//                }
//            })
//        }
//    }


//    override fun getNearbyRestaurants(
//        lat: String,
//        lon: String,
//        onSuccess: (restaurants: List<Restaurant>) -> Unit,
//        onFailure: (error: ResponseBody) -> Unit,
//        onException: (e: Exception) -> Unit
//    ) {
//        CoroutineScope(Dispatchers.IO).launch {
//            val response = retrofitService.getGeoCode(lat, lon)
//
//            withContext(Dispatchers.Main) {
//                try {
//                    if (response.isSuccessful) {
//                        response.body()?.let{
//                            onSuccess.invoke(it.nearby_restaurants.map {
//                                    nearbyRestaurant -> nearbyRestaurant.restaurant }
//                            )
//                        }
//                    } else {
//                        response.errorBody()?.let(onFailure)
//                    }
//                } catch (e: Exception) {
//                    onException.invoke(e)
//                }
//            }
//        }
//    }
//}