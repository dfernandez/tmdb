package com.example.tmdb.network

import com.example.tmdb.BuildConfig
import com.example.tmdb.network.responses.Token
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface RetrofitService {
    @GET("/3/authentication/token/new")

    suspend fun getNewToken(@Query("api_key") apiKey: String): Response<Token>
}