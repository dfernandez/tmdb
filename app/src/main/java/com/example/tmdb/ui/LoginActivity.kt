package com.example.tmdb.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.tmdb.BuildConfig
import com.example.tmdb.R
import com.example.tmdb.network.RetrofitService
import com.example.tmdb.network.responses.Token
import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjectionModule
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import retrofit2.Response
import javax.inject.Inject
import com.example.tmdb.MainActivity


class LoginActivity: AppCompatActivity() {

    @Inject lateinit var retrofitService: RetrofitService

    lateinit var webView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        webView = WebView(this)
        val webViewClient = object: WebViewClient(){
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                view?.loadUrl(request?.url.toString())

                val approved:Boolean = request?.url?.getQueryParameter("approved")?.toBoolean()?:false
                if (approved){
                    val requestToken = request?.url?.getQueryParameter("request_token")
                    val preferences = getSharedPreferences("tmdbPrefs", Context.MODE_PRIVATE)
                    preferences.edit().putString("token", requestToken).commit()

                    val intent:Intent = Intent(this@LoginActivity , MainActivity::class.java)
                    startActivity(intent)
                }
                return true
            }
        }
        webView.webViewClient = webViewClient
        webView.settings.javaScriptEnabled = true
        setContentView(webView)

    }

    override fun onResume() {
        super.onResume()
        login()



    }

    private fun login(){
        CoroutineScope(Dispatchers.IO).launch {
            val response: Response<Token> = retrofitService.getNewToken(BuildConfig.TMDB_API_KEY)
            withContext(Dispatchers.Main) {
                try {
                    if (response.isSuccessful) {
                        toast(response.isSuccessful.toString())
                        webView.loadUrl("https://themoviedb.org/authenticate/${response.body()?.requestToken}?redirect_to=http://tmdb.com/main")

                    } else {
                        toast("Error: ${response.code()} ${response.raw()}")
                    }
                } catch (e: HttpException) {
                    toast("Exception ${e.message}")
                } catch (e: Throwable) {
                    toast("Ooops: Something else went wrong")
                }
            }
        }
    }

    fun Context.toast(message: CharSequence) =
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}