package com.example.tmdb.di

import com.example.tmdb.BuildConfig
import com.example.tmdb.network.APIClient
import com.example.tmdb.network.RetrofitService
import dagger.Binds
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

//    @Singleton
//    @Binds
//    abstract fun apiClientImpl(apiClient: APIClientRetrofitImpl): APIClient

    @Singleton
    @Provides
    fun providesRetrofit(): RetrofitService {

        return Retrofit.Builder()
            .baseUrl(BuildConfig.TMDB_URL)
            .addConverterFactory(MoshiConverterFactory.create().asLenient())
            .client(getHTTPClientWithInterceptor())
            .build().create(RetrofitService::class.java)
    }

    @Module
    companion object{
//
//        @Singleton
//        @Provides
//        @JvmStatic
//        fun providesRetrofit(): RetrofitService {
//
//            return Retrofit.Builder()
//                .baseUrl(BuildConfig.TMDB_URL)
//                .addConverterFactory(MoshiConverterFactory.create().asLenient())
//                .client(getHTTPClientWithInterceptor())
//                .build().create(RetrofitService::class.java)
//        }

        private fun getHTTPClientWithInterceptor(): OkHttpClient {
            val logging = HttpLoggingInterceptor()
            logging.apply { logging.level = HttpLoggingInterceptor.Level.BODY  }
            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(logging)
            return httpClient.build()
        }
    }



}