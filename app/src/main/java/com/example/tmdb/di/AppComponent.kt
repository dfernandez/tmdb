package com.example.tmdb.di

import android.app.Application
import com.example.tmdb.TMDBApp
import com.example.tmdb.ui.LoginActivity
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class,  AndroidInjectionModule::class, ActivityModule::class, NetworkModule::class])
interface AppComponent {
    @Component.Builder
    interface Builder{
        @BindsInstance
        fun application(application: Application): Builder
        fun build():AppComponent
    }

    fun inject(tmdbApp: TMDBApp)
    fun inject(loginActivity: LoginActivity)
}