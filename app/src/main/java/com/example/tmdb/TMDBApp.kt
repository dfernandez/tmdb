package com.example.tmdb

import android.app.Activity
import android.app.Application
import com.example.tmdb.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class TMDBApp: Application(), HasActivityInjector {

    @Inject lateinit var androidInjector : DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder().application(this).build().inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> = androidInjector


}